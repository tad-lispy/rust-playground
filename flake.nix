{
  description = "The Rust language playground";

  inputs = {
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
    flake-utils = {
      url = "github:numtide/flake-utils";
    };
  };

  outputs = { self, nixpkgs, flake-utils, flake-compat }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
        };

        common-inputs = [
          pkgs.rustup
          pkgs.rust-analyzer
          pkgs.gcc

          # Other utilities
          pkgs.gnumake
          pkgs.jq
        ];

      in rec {
        defaultPackage = pkgs.stdenv.mkDerivation {
          buildInputs = common-inputs ++ [
          ];
          name = "rust-playground";
          src = self;
          buildPhase = "make dist";
          installPhase = "make install";
        };


        devShell = pkgs.mkShell {
          name = "rust-playground-development-shell";
          src = self;
          buildInputs = common-inputs ++ [
            pkgs.cachix
          ];
        };
      }
    );
}
