use rand::Rng;
use std::cmp::Ordering;
use std::io::{self, Write};

const MIN_GUESS: u8 = 1;
const MAX_GUESS: u8 = 255;

fn main() {
    let invalid_input_message = format!(
        "Please type a number between {} and {}",
        MIN_GUESS, MAX_GUESS
    );

    println!("Hello! Guess the number.");

    let secret_number = rand::thread_rng().gen_range(MIN_GUESS..=MAX_GUESS);
    // TODO: How to print the number in development, but not in production?
    // println!("The secret number is {}.", secret_number);

    loop {
        print!("Your guess: ");
        io::stdout().flush().expect("Flushing stdout failed");
        let mut guess = String::new();
        io::stdin()
            .read_line(&mut guess)
            .expect("Failed to read the input");

        match guess.trim().to_lowercase().as_str() {
            "" => {
                println!("Got enough already? Type 'quit' or continue guessing.");
                continue;
            }
            "quit" => {
                println!("Ok, bye!");
                println!("BTW, the number was {}", secret_number);
                break;
            }
            _ => (),
        }

        let guess: u8 = match guess.trim().parse() {
            Ok(number) => number,
            Err(_) => {
                println!("{}", invalid_input_message);
                continue;
            }
        };

        if guess < MIN_GUESS {
            println!("{}", invalid_input_message);
            continue;
        }
        if guess > MAX_GUESS {
            println!("{}", invalid_input_message);
            continue;
        }

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too low!"),
            Ordering::Greater => println!("Too high!"),
            Ordering::Equal => {
                println!("You got it!");
                break;
            }
        }
    }
}
